<?php 
/*
 * This file is part of Flarum.
 *
 * (c) Toby Zerner <toby.zerner@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Binh\Mentions\Listener;

use Flarum\Event\ConfigureFormatter;
use Illuminate\Contracts\Events\Dispatcher;

class FormatDiscussionMentions
{

    /**
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(ConfigureFormatter::class, [$this, 'configure']);

    }

    /**
     * @param ConfigureFormatter $event
     */
    public function configure(ConfigureFormatter $event)
    {
        $configurator = $event->configurator;

        $tagName = 'USERMENTION';

        $tag = $configurator->tags->add($tagName);
        $tag->attributes->add('id');
				$tag->attributes->add('user');


        $tag->template = '<a href="/d/{@id}" data-id="{@user}" class="UserMention">@<xsl:value-of select="@slug"/></a>';


        $configurator->Preg->match('/\B~(?<slug>[a-z0-9_-]+)#(?<id>([0-9]+))/i', $tagName);
    }

    
}
