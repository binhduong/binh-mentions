<?php

/*
 * This file is part of Flarum.
 *
 * (c) Toby Zerner <toby.zerner@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Binh\Mentions\Listener;
use Flarum\Event\PostWillBeSaved;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\View\Factory;


return function (Dispatcher $events, Factory $views) {
	
    $events->subscribe(Listener\AddClientAssets::class);
		//$events->subscribe(Listener\FormatDiscussionMentions::class);
    $events->subscribe(Listener\UpdateUserMentionsMetadata::class);

    $views->addNamespace('binh-mentions', __DIR__.'/views');
		/*$events->listen(PostWillBeSaved::class, function (PostWillBeSaved $event) {
        // do stuff before a post is saved
        $event->post->content = "[URL=/d/1]a b c[/URL]";
    });*/

};
