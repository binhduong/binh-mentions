import { extend } from 'flarum/extend';
import app from 'flarum/app';

import NotificationGrid from 'flarum/components/NotificationGrid';
import { getPlainContent } from 'flarum/utils/string';

import addPostMentionPreviews from 'binh/mentions/addPostMentionPreviews';
import addComposerAutocomplete from 'binh/mentions/addComposerAutocomplete';
import UserMentionedNotification from
'binh/mentions/components/UserMentionedNotification';

app.initializers.add('binh-mentions', function() {
	// For every mention of a post inside a post's content, set up a hover handler
	// that shows a preview of the mentioned post.
	addPostMentionPreviews();


	// After typing '~' in the composer, show a dropdown suggesting a bunch of
	// posts or users that the user could mention.
	addComposerAutocomplete();

	app.notificationComponents.userMentioned = UserMentionedNotification;
	// Add notification preferences.
	extend(NotificationGrid.prototype, 'notificationTypes', function(items) {
		items.add('userMentioned', {
			name : 'userMentioned',
			icon : 'at',
			label : app.translator.trans('flarum-mentions.forum.settings.notify_user_mentioned_label')
		});

	});

});
