var gulp = require('flarum-gulp');

gulp({
  modules: {
    'binh/mentions': 'src/**/*.js'
  },
  files: [
    'bower_components/textarea-caret-position/index.js',
    'bower_components/speakingurl/speakingurl.min.js'
  ]
});
